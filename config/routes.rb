Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  get 'pages/send_email'
  post 'pages/send_email'
  get 'pages/eng'
  # You can have the root of your site routed with "root"
  root 'pages#index'

  namespace :admin do
    root 'application#admin'
    resources :wines
    resources :events
    resources :availabilities
  end

end
