class CreateWines < ActiveRecord::Migration
  def change
    create_table :wines do |t|
      t.string :image
      t.string :kind
      t.string :alcohol
      t.string :name
      t.integer :year
      t.text :description
      t.string :category
      t.integer :position

      t.timestamps null: false
    end
  end
end
