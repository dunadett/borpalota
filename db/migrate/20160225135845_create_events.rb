class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :image
      t.string :name
      t.string :date_and_location
      t.date :start_date
      t.string :url_text
      t.string :url
      t.string :category

      t.timestamps null: false
    end
  end
end
