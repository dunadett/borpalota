class AddEnglishFields < ActiveRecord::Migration
  def change
    add_column :wines, :kind_eng, :string
    add_column :wines, :name_eng, :string
    add_column :wines, :description_eng, :text

    add_column :events, :name_eng, :string
    add_column :events, :date_and_location_eng, :string
  end
end
