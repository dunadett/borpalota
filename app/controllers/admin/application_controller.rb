# encoding: utf-8
class Admin::ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :check_authentication

  layout 'admin'

  def admin
  end

  private
  def check_authentication
    authenticate_or_request_with_http_basic do |user, password|
      user == 'DubiczAdmin' && password == 'B0rpal0ta'
    end
  end
end
