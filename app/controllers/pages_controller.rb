class PagesController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  layout 'application'

  before_action :authenticate unless Rails.env.development? || HOSTNAME == 'http://www.dubicz.hu'
  before_action :redirect_to_dubicz

  def index
  end

  def send_email
    UserMessage.user_message(params[:message]).deliver_now unless params[:message][:subject].present?
    redirect_to root_path
  end

  def eng
  end

  protected

  USERS = { 'DubiczAdmin' => 'B0rpal0ta'}
  def authenticate
    authenticate_or_request_with_http_digest do |name|
      USERS[name]
    end
  end

  def redirect_to_dubicz
    redirect_to "#{request.protocol}www.dubicz.hu#{request.fullpath}", :status => :moved_permanently unless request.host.in? %w(localhost www.dubicz.hu borpalota-staging.herokuapp.com)
  end
end
