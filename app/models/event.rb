class Event < ActiveRecord::Base

  CATEGORIES = [
        { name: 'Aktualitások'},
        { name: 'Események'}
    ]

  def is_facebook?
    url.downcase.include? 'facebook'
  end
end
