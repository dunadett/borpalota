class UserMessage < ApplicationMailer
  default from: 'no-reply@dubicz.hu'

  def user_message(message)
    @name = message[:name]
    @email = message[:email]
    @message = message[:message]
    mail(to: 'info@dubicz.hu', subject: 'Üzenet a weboldalról')
  end
end
