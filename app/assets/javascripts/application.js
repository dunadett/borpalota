// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require_directory ./vendor
//= require bootstrap
//= require_tree .

$(document).ready(function(){
    var $menuWithId = $('.menu[id]');
    if($menuWithId.length == 0) {
        $('.language a').removeClass('active');
        $('.language a:first-of-type').addClass('active');
    } else {
        $('.language a').removeClass('active');
        $('.language a:last-of-type').addClass('active');
    };

    $(document).on('click','.navbar-collapse.in',function(e) {
        if( $(e.target).is('a') ) {
            $(this).collapse('hide');
        }
    });

    $('.carousel').carousel()

    //age check
    $('section#age-check #yes').click(function(e){
        $('#age-check').fadeOut(500);
    });

    var cw = $('article').width();
    $('.wineries article').css({
        'height': 0.85*cw + 'px'
    });

    $('section#wineries article').hover(function(){
        $(this).toggleClass('focus');
    });

    $('#popup').hide()

    $('section#wineries #block-1').click(function(e){
        $('#popup').fadeIn(1000);
        $('#popup #article-1').addClass('active');
    });

    $('section#wineries #block-2').click(function(e){
        var target = $('#wine-speciality');
        $('html,body').animate({
            scrollTop: target.offset().top - 60
        }, 2000);
        return false;
    });

    $('section#wineries #block-3').click(function(e){
        $('#popup').fadeIn(1000);
        $('#popup #article-3').addClass('active');
    });

    $('section#wineries #block-4').click(function(e){
        $('#popup').fadeIn(1000);
        $('#popup #article-4').addClass('active');
    });

    $('section#wineries #block-5').click(function(e){
        var target = $('#thousand-fourteen');
        $('html,body').animate({
            scrollTop: target.offset().top - 60
        }, 2000);
        return false;
    });

    $('section#wineries #block-6').click(function(e){
        $('#popup').fadeIn(1000);
        $('#popup #article-6').addClass('active');
    });

    $('section#wineries #block-7').click(function(e){
        $('#popup').fadeIn(1000);
        $('#popup #article-7').addClass('active');
    });

    $('section#wineries #block-8').click(function(e){
        var target = $('#boraszat');
        $('html,body').animate({
            scrollTop: target.offset().top - 60
        }, 2000);
        return false;
    });

    $('section#wineries #block-9').click(function(e){
        var target = $('#csomagjaink');
        $('html,body').animate({
            scrollTop: target.offset().top - 60
        }, 2000);
        return false;
    });

    $('section#our-wines .wine').click(function(e){
        var wine = $(e.target).parents('.wine').attr('id');
        $('#popup').fadeIn(1000);
        $('#popup #' + wine +'-popup').addClass('wine active');
    });

    //close popups
    $('#popup').click(function(e) {
        if (!$(e.target).parents().andSelf().is('.album')) {
            $('#popup').fadeOut(1000);
            setTimeout(function() {
                $('#popup .album').removeClass('wine active');
            }, 1000);
        }
    });

    $('section#our-wines .wine').hover(function(){
        $(this).toggleClass('focus');
    });

    $('section#events .text-box a').bigTarget({
        clickZone: '.wine-tasting, .festival, .link'
    });
});

function validateForm() {
    var name = document.forms["form"]["name"].value;
    var email = document.forms["form"]["email"].value;
    var message = document.forms["form"]["message"].value;
    var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var has_error = false;

    if (name == null || name == "") {
        $('.name').addClass('error');
        has_error = true;
    } else {
        $('.name').removeClass('error');
    }
    if (!email.match(mailFormat)) {
        $('.email').addClass('error');
        has_error = true;
    } else {
        $('.email').removeClass('error');
    }
    if (message == null || message == "") {
        $('.message').addClass('error');
        has_error = true;
    } else {
        $('.message').removeClass('error');
    }
    if (has_error == true) {
        $('.submit').addClass('check-details');
        return false;
    }
    if (has_error == false) {
        $('.submit').removeClass('check-details');
        return true;
    }
};

$(document).on('submit', '#form', function(e){
    if (!validateForm()) {
        e.preventDefault();
    } else {
        $('#form').find("input[type=text], input[type=email], textarea").val("");
        $('#form').find("input[type=submit]").val("Köszönjük");
    };
});

//menu scroll page
$(function() {
  $('#menu a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 60
        }, 2000);
        return false;
      }
    }
  });
});
$(function() {
  $('#mobile-menu a[href*=#]:not([href=#])').click(function() {
    $('.navbar-collapse.in').collapse('hide');
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 40
        }, 3000);
        return false;
      }
    }
  });
});

(function() {
  $('#wine-speciality .wine .text-box').matchHeight();
})();