json.array!(@events) do |event|
  json.extract! event, :id, :image, :name, :date_and_location, :start_date, :url_text, :url, :category
  json.url admin_event_url(event, format: :json)
end
