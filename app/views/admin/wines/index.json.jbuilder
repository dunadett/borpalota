json.array!(@wines) do |wine|
  json.extract! wine, :id, :image, :kind, :alcohol, :name, :year, :description, :category, :position
  json.url admin_wine_url(wine, format: :json)
end
